#include <IRremotea.h>
#include <Wire.h>
#include <avr/pgmspace.h> //to use PROGMEM

//#define USE_LED

#define LEDMAX 255
#define LEDMIN 0
#define LEDSTEP 3
#define LEDSTEP2 20
#define LEDPIN 9
#define DELAY 20

#define SPKPIN 2

#define SLAVE_ADDRESS 0x04
int number = 0;
int state = 0;
int RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
decode_results results;
int last_ir_value = 0;
int current_brightness = 0;
int onWave = 0;

/*
 * Array for LED Gamma Correction.
 * See Reference 2 for more information.
*/
const uint8_t PROGMEM gamma[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };

/*
 * Get real LED value after gamma correction.
 */
int getLedValue(int value)
{
  if ((value> 255) || (value <0)) return 0;
#ifndef USE_LED
  return value;
#endif
#ifdef USE_LED
  return pgm_read_byte(&gamma[value]);
#endif
}

void setLedValue(int value)
{
  analogWrite(LEDPIN, getLedValue(value));  
}


void toggle_13()
{
  if (state == 0){
        digitalWrite(13, HIGH); // set the LED on
        state = 1;
      } 
      else{
        digitalWrite(13, LOW); // set the LED off
        state = 0;
      }
}

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  pinMode(13, OUTPUT);

  // initialize i2c as slave
  Wire.begin(SLAVE_ADDRESS);

  // define callbacks for i2c communication
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);
  
  pinMode(SPKPIN, OUTPUT);
}

void read_ir_data(){
  if (irrecv.decode(&results)) {
    
    if (results.value != 0xFFFFFFFF && results.value & 0x00FF0000 == 0x00FF0000) 
    {
      Serial.print("[IR]Recv: ");
    Serial.println(results.value, HEX);
      //Serial.println(", Verify OK");
      //Serial.println(results.value && 0xFF000000, HEX);
      last_ir_value = results.value;
    }

    if (results.value == 0xFFA25D)  //power
      toggle_13();
      
    if (results.value == 0xFF02FD)  //+
      {
        onWave = 0;
        current_brightness += LEDSTEP2;
        if (current_brightness > LEDMAX) current_brightness = LEDMAX;
        setLedValue(current_brightness);
      }
      
      if (results.value == 0xFF9867) //-
      {
        onWave = 0;
        current_brightness -= LEDSTEP2;
        if (current_brightness < LEDMIN) current_brightness = LEDMIN;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFFA857) // play
      {
        onWave = !onWave;
      }
      if (results.value == 0xFF6897) //0
      {
        onWave = 0;
        current_brightness = 0;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF30CF) //1
      {
        onWave = 0;
        current_brightness = 24;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF18E7) //2
      {
        onWave = 0;
        current_brightness = 48;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF7A85) //3
      {
        onWave = 0;
        current_brightness = 72;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF10EF) //4
      {
        onWave = 0;
        current_brightness = 96;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF38C7) //5
      {
        onWave = 0;
        current_brightness = 120;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF5AA5) //6
      {
        onWave = 0;
        current_brightness = 144;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF42BD) //7
      {
        onWave = 0;
        current_brightness = 168;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF4AB5) //8
      {
        onWave = 0;
        current_brightness = 192;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFF52AD) //9
      {
        onWave = 0;
        current_brightness = 216;
        setLedValue(current_brightness);
      }
      if (results.value == 0xFFE21D) // MENU
      {
        digitalWrite(SPKPIN, HIGH);
        delay(300);
        digitalWrite(SPKPIN, LOW);
      }
      
      
    irrecv.resume(); // Receive the next value
  }
}

void loop() {
  
  if (onWave == 1)
  {
    for(int fadeValue = LEDMIN ; fadeValue <= LEDMAX; fadeValue += LEDSTEP) { 
      read_ir_data();
    setLedValue(fadeValue);       
    delay(DELAY);                            
  } 

  for(int fadeValue = LEDMAX ; fadeValue >= LEDMIN; fadeValue -= LEDSTEP) { 
    read_ir_data();
    setLedValue(fadeValue);        
    delay(DELAY);                            
  } 
  }else {read_ir_data();}
}

// callback for received data
void receiveData(int byteCount){

  while(Wire.available()) {
    number = Wire.read();
    Serial.print("[I2C]Recv: 0x");
    Serial.println(number, HEX);

    if (number == 1){
      toggle_13();
    }
    if(number==254) {
      number = last_ir_value;
      last_ir_value = 0;
      Serial.print("[I2C]Send: 0x");
      Serial.println(number, HEX);
    }
  }
}

// callback for sending data
void sendData(){
  Wire.write(number);
}
